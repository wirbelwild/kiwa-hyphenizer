[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/hyphenizer)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/hyphenizer/v/stable)](https://packagist.org/packages/kiwa/hyphenizer)
[![Total Downloads](https://poser.pugx.org/kiwa/hyphenizer/downloads)](https://packagist.org/packages/kiwa/hyphenizer)
[![License](https://poser.pugx.org/kiwa/hyphenizer/license)](https://packagist.org/packages/kiwa/hyphenizer)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Hyphenizer

Perfect hyphenation for your Website. This library integrates the [Hyphenizer](https://www.hyphenizer.com) into your [Kiwa](https://www.kiwa.io) website.

If you don't run your website with Kiwa, you can do these steps by your own.

__Please note:__ The Hyphenizer API requires you to have a valid API token.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/hyphenizer). Add it to your project by running `$ composer require kiwa/hyphenizer`. 

## Usage

The static hyphenation works in three steps:

### 1. Extract all long words

To extract all long words from your website, run `$ php bin/console hyphenation:list:create` in your CLI.

The command will store the extracted words under `/hyphenation/hyphenation-words.php`.

### 2. Ask for their correct hyphenation

Ask the Hyphenizer API for the correct hyphenation. Run `$ php bin/console hyphenation:list:hyphenate` in your CLI.

The command will store the hyphenated words under `/hyphenation/hyphenation-words.json`.

### 3. Replace the original words with their hyphenated versions.

You can use the [Hyphenation library written in JavaScript](https://www.npmjs.com/package/kiwa-hyphenizer) to replace all the extracted words. In this case, you need to import your `hyphenation-words.json` before initializing the class: 

```javascript
import Hyphenation from "kiwa-hyphenizer";
import hyphenatedWords from "hyphenation/hyphenation-words.json";

const hyphenation = new Hyphenation(hyphenatedWords);
```

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
