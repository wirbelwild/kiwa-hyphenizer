<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Command;

use BitAndBlack\Helpers\XMLHelper;
use DateTime;
use DOMDocument;
use DOMNode;
use DOMXPath;
use Kiwa\Hyphenizer\Exception\FolderException;
use Kiwa\Hyphenizer\WordsExtractor;
use Kiwa\Page\PageList;
use Kiwa\Path;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HyphenationListCreateCommand extends Command
{
    private LoggerInterface $logger;

    public static string $commandName = 'hyphenation:list:create';

    private static string $wordsFile = 'hyphenation-words.php';

    public function __construct()
    {
        $this->logger = new NullLogger();
        parent::__construct();
    }

    public function configure(): self
    {
        $this
            ->setName(self::$commandName)
            ->setDescription('Searches in all cached files for long words.')
            ->addOption(
                'cache',
                'c',
                InputOption::VALUE_NONE,
                'Set this option to extract words from all cached files. This method is more accurate but requires to have all pages cached.'
            )
            ->addOption(
                'new',
                null,
                InputOption::VALUE_NONE,
                'Set this option to remove the currently existing words file and create a new one.'
            )
            ->addOption(
                'minWordLength',
                null,
                InputOption::VALUE_REQUIRED,
                'How many characters a word should have at least to be extracted.'
            )
        ;
        return $this;
    }

    /**
     * @throws FolderException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        $fromCache = $input->getOption('cache');
        $createNewFile = $input->getOption('new');

        /** @var string|null $minWordLength */
        $minWordLength = $input->getOption('minWordLength');

        if (null === $minWordLength) {
            $minWordLength = 12;
        }

        $wordsExtractor = new WordsExtractor((int) $minWordLength);

        $words = [];
        
        if ($fromCache) {
            $pageList = new PageList();
            $pageListCount = iterator_count($pageList);
            
            $cacheFiles = glob(Path::getCacheFolder() . DIRECTORY_SEPARATOR . '*.html');
            $cacheFilesCount = false !== $cacheFiles ? count($cacheFiles) : 0;

            if (false === $cacheFiles) {
                $io->error('No cache files found.');
                return Command::SUCCESS;
            }
            
            if ($pageListCount > $cacheFilesCount) {
                $io->warning('Found less cache files (' . $cacheFilesCount . ') then defined pages (' . $pageListCount . '). This could mean, that some cache files are missing and your word list won\'t be complete.');
            }
            
            $progressBar = new ProgressBar($output, $cacheFilesCount);
            $progressBar->start();
            
            foreach ($cacheFiles as $page) {
                $progressBar->advance();
                $fileContent = (string) file_get_contents($page);
                $content = $this->extractContent($fileContent);
                array_push($words, ...$wordsExtractor->getWords($content));
            }
            
            $progressBar->finish();
            $io->writeln('');
        }
        
        if (!$fromCache) {
            $phtmlFiles = glob(Path::getHTMLFolder() . DIRECTORY_SEPARATOR . '*.phtml');
            $phtmlFilesCount = false !== $phtmlFiles ? count($phtmlFiles) : 0;

            if (false === $phtmlFiles) {
                $io->error('No phtml files found.');
                return Command::SUCCESS;
            }
            
            $progressBar = new ProgressBar($output, $phtmlFilesCount);
            $progressBar->start();
            
            foreach ($phtmlFiles as $phtmlFile) {
                $progressBar->advance();
                $fileContent = $this->loadFile($phtmlFile);
                $content = $this->extractContent($fileContent);
                array_push($words, ...$wordsExtractor->getWords($content));
            }
        
            $progressBar->finish();
            $io->writeln('');
        }

        $words = array_unique($words);
        $wordsCount = count($words);

        natcasesort($words);
        
        $io->writeln('Found ' . $wordsCount . ' words.');
        
        $dir = dirname(self::getWordsFile());
        
        if (!file_exists($dir)
            && !mkdir($dir)
            && !is_dir($dir)
        ) {
            throw new FolderException($dir);
        }

        $gitignore = self::getRootFolder() . DIRECTORY_SEPARATOR . '.gitignore';
        
        if (!file_exists($gitignore)) {
            file_put_contents(
                $gitignore,
                ''
            );
        }

        if (false === $createNewFile && file_exists(self::getWordsFile())) {
            $currentWords = include self::getWordsFile();

            if (!is_array($currentWords)) {
                $currentWords = [];
            }
            
            array_push($words, ...$currentWords);
            $words = array_unique($words);
            natcasesort($words);
        }
        
        $success = false !== file_put_contents(
            self::getWordsFile(),
            '<?php ' . PHP_EOL .
                '/**' . PHP_EOL .
                ' * This file has been @auto-generated by `kiwa/hyphenizer` on ' . (new DateTime('now'))->format('Y-m-d H:i:s') . '.' . PHP_EOL .
                ' */' . PHP_EOL .
                'return [' . PHP_EOL .
                '    \'' . implode(
                    '\',' . PHP_EOL . '    \'',
                    $words
                ) . '\',' . PHP_EOL .
                '];'
        );

        if ($success) {
            $io->success('Created file://' . self::getWordsFile() . '.');
            $io->writeln('It contains ' . $wordsCount . ' words with ' . $wordsExtractor->getMinWordLength() . ' and more characters. ' . PHP_EOL .
                'Please review the list: maybe it contains words you don\'t want to hyphenate and ' . PHP_EOL .
                'maybe you can find a typo that you didn\'t notice yet.' . PHP_EOL);
            return Command::SUCCESS;
        }

        $io->error('Couldn\'t create file "' . self::getWordsFile() . '".');
        return Command::SUCCESS;
    }

    public static function getRootFolder(): string
    {
        return Path::getRootFolder() . DIRECTORY_SEPARATOR . 'hyphenation';
    }
    
    public static function getWordsFile(): string
    {
        return self::getRootFolder() . DIRECTORY_SEPARATOR . self::$wordsFile;
    }

    private function loadFile(string $fileName): string
    {
        ob_start();
        include $fileName;
        return (string) ob_get_clean();
    }

    private function extractContent(string $fileContent): string
    {
        if (empty($fileContent)) {
            $this->logger->debug('fileContent ' . $fileContent . ' empty');
            return '';
        }
        
        $domDocument = new DOMDocument();
        XMLHelper::loadHTML($domDocument, $fileContent);

        $body = $domDocument->getElementsByTagName('body');
        
        $xpath = new DOMXPath($domDocument);

        /**
         * When a body tag exists, we only extract words from there.
         */
        $textNodes = 0 === $body->count()
            ? $xpath->query('//text()')
            : $xpath->query('//body//text()')
        ;

        if (false === $textNodes) {
            $this->logger->debug('Body has no text nodes.');
            $textNodes = [];
        }

        $content = '';

        /** @var DOMNode $textNode */
        foreach ($textNodes as $textNode) {
            $content .= ' ' . $textNode->nodeValue;
        }
        
        return $content;
    }
}
