<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Command;

use JsonException;
use Kiwa\Hyphenizer\Exception\FolderException;
use Kiwa\Hyphenizer\Exception\MissingAPITokenException;
use Kiwa\Hyphenizer\Hyphenation;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @see \Kiwa\Hyphenizer\Tests\Command\HyphenationListHyphenateCommandTest
 */
class HyphenationListHyphenateCommand extends Command
{
    private LoggerInterface $logger;

    private ?string $token;

    public function __construct()
    {
        $this->logger = new NullLogger();
        $this->token = $_ENV['HYPHENIZER_API_TOKEN'] ?? null;
        parent::__construct();
    }

    public function configure(): self
    {
        $this
            ->setName('hyphenation:list:hyphenate')
            ->setDescription('Requests the Hyphenizer API for the correct hyphenation word by word.')
            ->addOption(
                'api-token',
                't',
                InputOption::VALUE_REQUIRED,
                'The Hyphenizer API Token.'
            )
        ;
        return $this;
    }

    /**
     * @throws FolderException
     * @throws JsonException
     * @throws MissingAPITokenException
     * @throws ClientExceptionInterface
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /** @var string|null $apiToken */
        $apiToken = $input->getOption('api-token');
        
        if (null !== $apiToken) {
            $this->token = (string) $apiToken;
        }
        
        if (null === $this->token) {
            throw new MissingAPITokenException();
        }
        
        $wordsFile = HyphenationListCreateCommand::getWordsFile();

        if (!file_exists($wordsFile)) {
            $io->error(
                'The file with words doesn\'t exist under "' . $wordsFile . '". ' .
                'You need to create it with the "' . HyphenationListCreateCommand::$commandName . '" command at first.'
            );
            return Command::FAILURE;
        }
        
        $words = include $wordsFile;
        $wordsCount = is_countable($words) ? count($words) : 0;

        $io->writeln('Found ' . $wordsCount . ' words.');

        $hyphenatedWords = [];
        
        $progressBar = new ProgressBar($output, $wordsCount);
        $progressBar->start();

        $hyphenation = new Hyphenation($this->token);

        foreach ($words as $word) {
            $progressBar->advance();
            
            $wordHyphenated = $hyphenation->getWordHyphenated($word);

            if (!str_contains($wordHyphenated, '|')) {
                continue;
            }
            
            $hyphenatedWords[$word] = $wordHyphenated;
        }

        $progressBar->finish();
        $io->writeln('');

        $dir = dirname(self::getWordsFile());

        if (!file_exists($dir)
            && !mkdir($dir)
            && !is_dir($dir)
        ) {
            throw new FolderException($dir);
        }

        try {
            $success = false !== file_put_contents(
                self::getWordsFile(),
                json_encode($hyphenatedWords, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
            );
        } catch (JsonException $exception) {
            $this->logger->error($exception);
            $success = false;
        }

        $success
            ? $io->success('Created file://' . self::getWordsFile() . '.')
            : $io->error('Couldn\'t create file "' . self::getWordsFile() . '".')
        ;

        if ([] !== $wordsWithTypos = $hyphenation->getWordsWithTypos()) {
            $io->warning('Hyphenizer detected some typos, please proof these words.');
            $io->writeln('The words are:');

            foreach ($wordsWithTypos as $word) {
                $io->writeln('- ' . $word);
            }
        }

        return Command::SUCCESS;
    }

    public static function getWordsFile(): string
    {
        return HyphenationListCreateCommand::getRootFolder() . DIRECTORY_SEPARATOR . 'hyphenation-words.json';
    }
}
