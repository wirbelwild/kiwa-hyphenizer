<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer;

/**
 * @see \Kiwa\Hyphenizer\Tests\WordsExtractorTest
 */
class WordsExtractor
{
    private int $minWordLength;

    public function __construct(?int $minWordLength = null)
    {
        $this->minWordLength = $minWordLength ?? 12;
    }

    /**
     * @return array<int, string>
     */
    public function getWords(string $content): array
    {
        $words = [];

        preg_replace_callback(
            '/(\w+:\w+)|(\w+\*\w+)|\w+/u',
            function (array $match) use (&$words) {
                $word = $match[0];

                if (mb_strlen($word) < $this->minWordLength) {
                    return '';
                }

                $words[] = $word;
                return '';
            },
            $content
        );

        return $words;
    }

    /**
     * @return int
     */
    public function getMinWordLength(): int
    {
        return $this->minWordLength;
    }
}
