<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Exception;

use Kiwa\Hyphenizer\Exception;

class MissingAPITokenException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Please provide an API token for the Hyphenizer API as environment variable ' .
            'named "HYPHENIZER_API_TOKEN" or as argument when running this command.'
        );
    }
}
