<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Exception;

use Kiwa\Hyphenizer\Exception;

class FolderException extends Exception
{
    /**
     * @param string $dir
     */
    public function __construct(string $dir)
    {
        parent::__construct('Directory "' . $dir . '" was not created');
    }
}
