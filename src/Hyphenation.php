<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer;

use Fig\Http\Message\StatusCodeInterface;
use Http\Discovery\Psr18Client;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;

class Hyphenation
{
    private int $minScoreRequired;

    private Psr18Client $client;

    private string $token;

    /**
     * @var array<int, string>
     */
    private array $wordsWithTypos = [];

    public function __construct(string $token, int $minScoreRequired = 50)
    {
        $this->client = new Psr18Client();
        $this->token = $token;
        $this->minScoreRequired = $minScoreRequired;
    }

    /**
     * @param string $word
     * @return string
     * @throws JsonException
     * @throws ClientExceptionInterface
     */
    public function getWordHyphenated(string $word): string
    {
        $query = http_build_query([
            'token' => $this->token,
        ]);

        $request = $this->client->createRequest('GET', 'https://api.hyphenizer.com/v2/words/' . $word . '?' . $query);
        $response = $this->client->sendRequest($request);

        if (StatusCodeInterface::STATUS_OK !== $response->getStatusCode()) {
            return $word;
        }

        $responseBody = $response->getBody()->getContents();

        /**
         * @var array{
         *     status: int,
         *     messages: array<int, string>,
         *     payload: array<string,
         *         array<int, array{
         *             score: int,
         *             hyphenation: string,
         *             approved: bool,
         *             hasTypo: bool,
         *         }>
         *     >
         * } $responseDecoded
         */
        $responseDecoded = json_decode($responseBody, true, 512, JSON_THROW_ON_ERROR);

        $wordHyphenated = $responseDecoded['payload'][$word][0] ?? null;

        if (null === $wordHyphenated) {
            return $word;
        }

        if (true === $wordHyphenated['hasTypo']) {
            $this->wordsWithTypos[] = $word;
        }

        if ($wordHyphenated['score'] < $this->minScoreRequired) {
            return $word;
        }

        return $wordHyphenated['hyphenation'];
    }

    /**
     * @return array<int, string>
     */
    public function getWordsWithTypos(): array
    {
        return $this->wordsWithTypos;
    }
}
