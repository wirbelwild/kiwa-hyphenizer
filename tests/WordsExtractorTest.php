<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Tests;

use Generator;
use Kiwa\Hyphenizer\WordsExtractor;
use PHPUnit\Framework\TestCase;

class WordsExtractorTest extends TestCase
{
    /**
     * @dataProvider getWords
     * @param string $input
     * @param array<int, string> $outputExpected
     * @return void
     */
    public function testGetWords(string $input, array $outputExpected): void
    {
        $wordsExtractor = new WordsExtractor();

        self::assertSame(
            $outputExpected,
            $wordsExtractor->getWords($input)
        );
    }

    public static function getWords(): Generator
    {
        yield [
            'Ein langer Satz für alle Mutmacher:innen, Mütmacher*innen und Mütmacher_innen',
            [
                'Mutmacher:innen',
                'Mütmacher*innen',
                'Mütmacher_innen',
            ],
        ];

        yield [
            'Datenschutzerklärung DATENSCHUTZERKLÄRUNG',
            [
                'Datenschutzerklärung',
                'DATENSCHUTZERKLÄRUNG',
            ],
        ];
    }
}
