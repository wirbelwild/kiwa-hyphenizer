<?php

/**
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace Kiwa\Hyphenizer\Tests\Command;

use Kiwa\Hyphenizer\Command\HyphenationListHyphenateCommand;
use Kiwa\Hyphenizer\Exception\MissingAPITokenException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class HyphenationListHyphenateCommandTest extends TestCase
{
    public function testThrowsMissingAPITokenException(): void
    {
        $this->expectException(MissingAPITokenException::class);

        $hyphenationListHyphenateCommand = new HyphenationListHyphenateCommand();
        
        $commandTester = new CommandTester($hyphenationListHyphenateCommand);
        $commandTester->execute([]);
    }
}
